import os

import environ

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

env = environ.Env()

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# --------------------------------------------------------------------------------------------------
# - BASE -------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

SECRET_KEY = env('SECRET_KEY', default='!!! SET SECRET KEY !!!')

DEBUG = env.bool('DEBUG', default=True)

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=['localhost', '127.0.0.1'])


DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

THIRD_PART_APPS = [
    'django_extensions',
    'django_s3_storage',
    'whitenoise.runserver_nostatic',
    'drf_yasg',
    'corsheaders',
    'rest_framework',
    'rest_framework.authtoken',
]

LOCAL_APPS = [
    'ithub.main',
    'ithub.users',
    'ithub.professions',
    'ithub.cases',
    'ithub.projects',
    'ithub.students',
    'ithub.mentors',
    'ithub.education',
]

INSTALLED_APPS = THIRD_PART_APPS + LOCAL_APPS + DJANGO_APPS


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = 'ithub.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'ithub.wsgi.application'

# --------------------------------------------------------------------------------------------------
# - DATABASES --------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

DATABASES = {
    'default': env.db('DATABASE_URL', default='sqlite:////%s/db.sqlite3' % BASE_DIR)
}

# --------------------------------------------------------------------------------------------------
# - AUTHENTICATION ---------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'users.User'

# -----------------------------------------------------------------------------------------------
# - SECURITY ------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------

if not DEBUG:
    SECURE_CONTENT_TYPE_NOSNIFF = True
    SECURE_BROWSER_XSS_FILTER = True
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    X_FRAME_OPTIONS = 'DENY'

# --------------------------------------------------------------------------------------------------
# - INTERNATIONALIZATION ---------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

LANGUAGE_CODE = 'ru-RU'

USE_I18N = True

USE_L10N = True

USE_TZ = False

# --------------------------------------------------------------------------------------------------
# - STATIC FILES -----------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

MEDIA_URL = '/uploads/'
AWS_ACCESS_KEY_ID = env('AWS_ACCESS_KEY_ID', default=None)
if AWS_ACCESS_KEY_ID is not None:
    DEFAULT_FILE_STORAGE = 'django_s3_storage.storage.S3Storage'
    AWS_S3_REGION = env('AWS_REGION', default='ru-central1')
    AWS_SECRET_ACCESS_KEY = env('AWS_SECRET_ACCESS_KEY')
    AWS_S3_BUCKET_NAME = env('AWS_S3_BUCKET_NAME')
    AWS_S3_ENDPOINT_URL = env('AWS_S3_ENDPOINT_URL', default=None)
else:
    MEDIA_ROOT = os.path.join(BASE_DIR, 'uploads')


# --------------------------------------------------------------------------------------------------
# - SENTRY TRACKING --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

if env('SENTRY_DSN', default=None):
    sentry_sdk.init(
        dsn=env('SENTRY_DSN'),
        integrations=[DjangoIntegration()],
        environment=env('SENTRY_ENV', default='staging')
    )

# --------------------------------------------------------------------------------------------------
# - EMAIL ------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

if env('MAILGUN_API_KEY', default=None):
    ANYMAIL = {
        'MAILGUN_API_KEY': env('MAILGUN_API_KEY'),
        'MAILGUN_SENDER_DOMAIN': env('MAILGUN_SENDER_DOMAIN'),
    }
    EMAIL_BACKEND = 'anymail.backends.mailgun.EmailBackend'

DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL', default='webmaster@localhost')
SERVER_EMAIL = env('SERVER_EMAIL', default='root@localhost')

# --------------------------------------------------------------------------------------------------
# - CELERY -----------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

CELERY_BROKER_URL = env('RABBITMQ_URL', default='localhost')
CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['application/json']

# --------------------------------------------------------------------------------------------------
# - TESTS ------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

TEST_RUNNER = 'ithub.utils.test_runner.CeleryTestSuiteRunner'

# --------------------------------------------------------------------------------------------------
# - DRF --------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------

REST_FRAMEWORK = {
    'EXCEPTION_HANDLER': 'ithub.api.exceptions_handlers.exception_handler',
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
}

SWAGGER_SETTINGS = {
    'DEFAULT_AUTO_SCHEMA_CLASS': 'ithub.api.swagger.inspectors.SwaggerAutoSchema',
    'USE_SESSION_AUTH': False,
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header'
        }
    },
    'PERSIST_AUTH': True,
}
