import os
from hashlib import md5

from django.utils import timezone


def upload_to(instance, filename):
    now = timezone.now()

    ext = os.path.splitext(filename)[1]
    timestamp = str(now.time())
    encode_filename = md5((filename + timestamp).encode('utf-8')).hexdigest() + ext

    app_path = os.path.join(instance._meta.app_label, instance._meta.model_name)
    dir_path = '%d-%d/%d' % (now.year, now.month, now.day)
    basedir = os.path.join(app_path, dir_path)

    return os.path.join(basedir, encode_filename)
