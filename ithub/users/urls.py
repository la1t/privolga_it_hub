from django.urls import path

from . import views

app_name = 'users'

urlpatterns = [
    path('auth/', views.AuthenticateView.as_view(), name='authenticate'),
    path('me/', views.ProfileView.as_view(), name='profile'),
    path('me/select-case/', views.SelectCaseView.as_view(), name='select-case'),
]
