from rest_framework import status, generics, permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from .models import UserRoles, SelectedCase
from .serializers import SelectCaseSerializer, ProfileSerializer


class AuthenticateView(APIView):
    serializer_class = AuthTokenSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, context={'request': self.request}, **kwargs)

    @swagger_auto_schema(
        request_body=AuthTokenSerializer,
        responses={
            status.HTTP_200_OK: openapi.Response(
                'OK',
                schema=openapi.Schema(type=openapi.TYPE_OBJECT, properties={
                    'token': openapi.Schema(type=openapi.TYPE_STRING),
                    'role': openapi.Schema(type=openapi.TYPE_STRING, enum=UserRoles.values)
                })
            )
        }
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key, 'role': user.role})


class SelectCaseView(APIView):
    serializer_class = SelectCaseSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    @swagger_auto_schema(
        request_body=SelectCaseSerializer,
        responses={
            status.HTTP_204_NO_CONTENT: openapi.Response('OK')
        }
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        case = serializer.validated_data['case']
        selected_case = SelectedCase.objects.create(case=case)
        request.user.selected_case = selected_case
        request.user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProfileView(generics.RetrieveAPIView):
    serializer_class = ProfileSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        return self.request.user

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)
