from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'ithub.users'
    verbose_name = 'Пользователи'
