# Generated by Django 3.1.1 on 2020-09-13 02:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cases', '0001_initial'),
        ('projects', '0004_auto_20200912_2239'),
        ('users', '0004_auto_20200912_2239'),
    ]

    operations = [
        migrations.CreateModel(
            name='SelectedCase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('case', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cases.case')),
                ('project', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='projects.project')),
            ],
        ),
        migrations.AddField(
            model_name='user',
            name='selected_case',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, to='users.selectedcase'),
        ),
    ]
