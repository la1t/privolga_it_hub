# Generated by Django 3.1.1 on 2020-09-12 12:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='email',
        ),
        migrations.AddField(
            model_name='user',
            name='role',
            field=models.CharField(choices=[('student', 'Student'), ('mentor', 'Mentor')], default='student', max_length=7),
            preserve_default=False,
        ),
    ]
