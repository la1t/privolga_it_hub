from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager

from ithub.utils.choices import count_max_length


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, password, **extra_fields):
        """
        Creates and saves a User with the given email,and password.
        """
        user = self.model(**extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, password=None, **extra_fields):
        return self._create_user(password, **extra_fields)

    def create_superuser(self, password, **extra_fields):
        extra_fields.setdefault('role', UserRoles.MENTOR)
        return self._create_user(password=password, **extra_fields)


class UserRoles(models.TextChoices):
    STUDENT = 'student'
    MENTOR = 'mentor'


class User(AbstractUser):
    role = models.CharField('Роль', choices=UserRoles.choices, max_length=count_max_length(UserRoles))
    email = None
    user_permissions = None
    groups = None

    selected_case = models.OneToOneField('SelectedCase', on_delete=models.SET_NULL, null=True, verbose_name='Выбранный кейс')

    num_scores = models.PositiveIntegerField('Кол-во баллов студента', default=0)

    objects = UserManager()

    REQUIRED_FIELDS = []

    @property
    def is_staff(self):
        return self.role == UserRoles.MENTOR

    @property
    def is_superuser(self):
        return self.role == UserRoles.MENTOR


class SelectedCase(models.Model):
    case = models.ForeignKey('cases.Case', on_delete=models.CASCADE)
    project = models.ForeignKey('projects.Project', on_delete=models.CASCADE, null=True)
