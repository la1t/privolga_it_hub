from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as _UserAdmin
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _

from rest_framework.authtoken.models import Token

from . import models

admin.site.unregister(Group)
admin.site.unregister(Token)


@admin.register(models.User)
class UserAdmin(_UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ('last_login', 'date_joined')
    list_filter = ()
    list_display = ('username', 'first_name', 'last_name')
    filter_horizontal = ()
