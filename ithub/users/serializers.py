from rest_framework import serializers

from ithub.cases.models import Case
from ithub.projects.models import Team

from .models import User, SelectedCase


class SelectCaseSerializer(serializers.Serializer):
    case = serializers.PrimaryKeyRelatedField(queryset=Case.objects.all())


class ProfileParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'first_name', 'last_name',
        )


class ProfileTeamSerializer(serializers.ModelSerializer):
    participants = ProfileParticipantSerializer(read_only=True, many=True)
    mentor = ProfileParticipantSerializer(read_only=True)

    class Meta:
        model = Team
        fields = (
            'name', 'participants', 'mentor',
        )


class ProfileCaseSerializer(serializers.ModelSerializer):
    direction = serializers.CharField(source='profession.direction')

    class Meta:
        model = Case
        fields = (
            'name', 'direction', 'picture'
        )


class SelectedCaseSerializer(serializers.ModelSerializer):
    case = ProfileCaseSerializer(read_only=True)
    team = ProfileTeamSerializer(read_only=True, required=False, source='project.team')

    class Meta:
        model = SelectedCase
        fields = (
            'case', 'team',
        )


class ProfileSerializer(serializers.ModelSerializer):
    selected_case = SelectedCaseSerializer(read_only=True, required=False)

    class Meta:
        model = User
        fields = (
            'first_name', 'last_name', 'role', 'num_scores', 'selected_case',
        )
