from django.apps import AppConfig


class CasesConfig(AppConfig):
    name = 'ithub.cases'
    verbose_name = 'Кейсы'
