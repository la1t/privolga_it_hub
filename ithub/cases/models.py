from django.db import models

from ithub.utils.files import upload_to


class Case(models.Model):
    name = models.CharField('Название', max_length=150)
    picture = models.ImageField('Изображение', upload_to=upload_to)
    profession = models.ForeignKey('professions.Profession', on_delete=models.CASCADE, verbose_name='Профессия')
    skills = models.ManyToManyField('education.Skill',
                                    verbose_name='Набор навыков, которыми овладеет'
                                                 ' студент для выполнения кейса')
    num_scores = models.PositiveIntegerField('Количество баллов, которые получают студенты за выполнение кейса')

    class Meta:
        verbose_name = 'Кейс'
        verbose_name_plural = 'Кейсы'

    def __str__(self):
        return self.name
