from django.contrib import admin

from . import models


@admin.register(models.Case)
class CaseAdmin(admin.ModelAdmin):
    pass
