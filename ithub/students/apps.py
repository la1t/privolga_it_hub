from django.apps import AppConfig


class StudentsConfig(AppConfig):
    name = 'ithub.students'
    verbose_name = 'Студенты'
