from django.contrib.auth.forms import UserCreationForm, UserChangeForm, UsernameField

from .models import Student


class StudentCreationForm(UserCreationForm):
    class Meta:
        model = Student
        fields = ("username",)
        field_classes = {'username': UsernameField}


class StudentChangeForm(UserChangeForm):
    class Meta:
        model = Student
        fields = '__all__'
        field_classes = {'username': UsernameField}
