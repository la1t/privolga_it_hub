from django.db import models

from ithub.users.models import User, UserRoles


class Student(User):
    class Meta:
        verbose_name = 'Студент'
        verbose_name_plural = 'Студенты'

    def save(self, *args, **kwargs):
        self.role = UserRoles.STUDENT
        super().save(*args, **kwargs)
