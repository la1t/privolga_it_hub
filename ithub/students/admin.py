from django.contrib import admin

from .forms import StudentCreationForm, StudentChangeForm
from .models import Student


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    form = StudentChangeForm
    add_form = StudentCreationForm

    def get_form(self, request, obj=None, change=False, **kwargs):
        if obj is None:
            return self.add_form
        return super().get_form(request, obj=obj, change=change, **kwargs)
