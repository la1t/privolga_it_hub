from django.db import models

from ithub.utils.files import upload_to


class Skill(models.Model):
    name = models.CharField('Название', max_length=100)
    num_scores = models.PositiveIntegerField('Кол-во баллов, которые ученик получает после получения навыка')

    class Meta:
        verbose_name = 'Умение'
        verbose_name_plural = 'Умения'

    def __str__(self):
        return self.name


class Lesson(models.Model):
    name = models.CharField('Название', max_length=100)
    desc = models.TextField('Краткое описание')
    skills = models.ManyToManyField('Skill', verbose_name='Умения, вырабатываемые на занятии')
    conference_link = models.URLField('Ссылка на конференцию', max_length=150, blank=True, null=True)

    class Meta:
        verbose_name = 'Семинар'
        verbose_name_plural = 'Семинары'

    def __str__(self):
        return self.name


class LessonAttachment(models.Model):
    lesson = models.ForeignKey('Lesson', on_delete=models.CASCADE)
    name = models.CharField('Название', max_length=150)
    file = models.FileField('Файл', upload_to=upload_to)

    class Meta:
        verbose_name = 'Вложение в семинар'
        verbose_name_plural = 'Вложения в семинар'

    def __str__(self):
        return self.name
