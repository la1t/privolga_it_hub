from django.contrib import admin

from . import models


class AttachmentInline(admin.TabularInline):
    model = models.LessonAttachment


@admin.register(models.Lesson)
class LessonAdmin(admin.ModelAdmin):
    inlines = [AttachmentInline]


@admin.register(models.Skill)
class SkillAdmin(admin.ModelAdmin):
    pass
