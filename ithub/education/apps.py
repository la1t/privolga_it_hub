from django.apps import AppConfig


class EducationConfig(AppConfig):
    name = 'ithub.education'
    verbose_name = 'Образование'
