from django.apps import AppConfig


class MentorsConfig(AppConfig):
    name = 'ithub.mentors'
    verbose_name = 'Менторы'
