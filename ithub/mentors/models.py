from django.db import models

from ithub.users.models import User, UserRoles


class Mentor(User):
    class Meta:
        verbose_name = 'Ментор'
        verbose_name_plural = 'Менторы'

    def save(self, *args, **kwargs):
        self.role = UserRoles.MENTOR
        super().save(*args, **kwargs)
