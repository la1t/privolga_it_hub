from django.contrib.auth.forms import UserCreationForm, UserChangeForm, UsernameField

from .models import Mentor


class MentorCreationForm(UserCreationForm):
    class Meta:
        model = Mentor
        fields = ("username",)
        field_classes = {'username': UsernameField}


class MentorChangeForm(UserChangeForm):
    class Meta:
        model = Mentor
        fields = '__all__'
        field_classes = {'username': UsernameField}
