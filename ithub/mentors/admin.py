from django.contrib import admin

from .forms import MentorCreationForm, MentorChangeForm
from .models import Mentor


@admin.register(Mentor)
class MentorAdmin(admin.ModelAdmin):
    form = MentorChangeForm
    add_form = MentorCreationForm

    def get_form(self, request, obj=None, change=False, **kwargs):
        if obj is None:
            return self.add_form
        return super().get_form(request, obj=obj, change=change, **kwargs)

