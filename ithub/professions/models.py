from django.db import models

from ithub.utils.files import upload_to


class Direction(models.Model):
    name = models.CharField('Название', max_length=100)

    class Meta:
        verbose_name = 'Направление'
        verbose_name_plural = 'Направления'

    def __str__(self):
        return self.name


class Profession(models.Model):
    name = models.CharField('Название', max_length=100)
    picture = models.ImageField('Изображение', upload_to=upload_to)
    desc = models.TextField('Описание')
    duration = models.PositiveIntegerField('Продолжительность (в часах)')
    direction = models.ForeignKey('Direction', on_delete=models.CASCADE, verbose_name='Направление')
    skills = models.ManyToManyField('education.Skill', verbose_name='Набор навыков, необходимых в профессии')

    class Meta:
        verbose_name = 'Профессия'
        verbose_name_plural = 'Профессии'

    def __str__(self):
        return self.name
