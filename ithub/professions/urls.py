from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register('professions', views.ProfessionViewSet)
router.register('directions', views.DirectionViewSet)

urlpatterns = router.urls
