from django.apps import AppConfig


class ProfessionsConfig(AppConfig):
    name = 'ithub.professions'
    verbose_name = 'Направления обучения'
