from django.contrib import admin

from . import models


@admin.register(models.Direction)
class DirectionAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Profession)
class ProfessionAdmin(admin.ModelAdmin):
    pass
