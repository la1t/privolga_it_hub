from rest_framework import serializers

from ithub.cases.models import Case

from .models import Direction, Profession


class DirectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Direction
        fields = ('id', 'name')


class CaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Case
        fields = ('id', 'name', 'picture')


class ProfessionSerializer(serializers.ModelSerializer):
    cases = CaseSerializer(source='case_set', read_only=True, many=True)

    class Meta:
        model = Profession
        fields = ('id', 'name', 'picture', 'desc', 'duration', 'direction', 'cases')
