from django.apps import AppConfig


class ProjectsConfig(AppConfig):
    name = 'ithub.projects'
    verbose_name = 'Проекты'
