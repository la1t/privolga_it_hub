from django.shortcuts import get_object_or_404
from django.utils.functional import cached_property

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework import generics, permissions, status
from rest_framework.views import APIView
from rest_framework.response import Response

from .serializers import ProjectSerializer, TaskSerializer, MoveToSerializer
from .models import Project, Task


class ProjectView(generics.RetrieveAPIView):
    model = Project
    serializer_class = ProjectSerializer
    permission_classes = (permissions.IsAuthenticated,)


class GetProjectMixin:

    def get_project(self):
        return get_object_or_404(Project, pk=self.kwargs['project_pk'])

    project = cached_property(get_project)


class BoardView(GetProjectMixin, generics.ListCreateAPIView):
    serializer_class = TaskSerializer

    def perform_create(self, serializer):
        serializer.save(project=self.project)

    def get_queryset(self):
        return self.project.task_set.all()


class TaskMoveToView(GetProjectMixin, APIView):
    serializer_class = MoveToSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer_class(*args, **kwargs)

    @swagger_auto_schema(
        request_body=MoveToSerializer,
        responses={
            status.HTTP_204_NO_CONTENT: openapi.Response('OK')
        }
    )
    def post(self, request, *args, **kwargs):
        task = get_object_or_404(Task, pk=self.kwargs['pk'])
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        task.move_to(serializer.validated_data['target'])
        return Response(status=status.HTTP_204_NO_CONTENT)
