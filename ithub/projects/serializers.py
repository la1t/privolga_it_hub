from rest_framework import serializers

from ithub.users.models import User
from ithub.cases.models import Case

from .models import Team, Project, Task


class ProjectParticipantSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class TeamSerializer(serializers.ModelSerializer):
    participants = ProjectParticipantSerializer(many=True, read_only=True)
    mentor = ProjectParticipantSerializer(read_only=True)

    class Meta:
        model = Team
        fields = ('participants', 'mentor')


class ProjectCaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Case
        fields = ('name',)


class ProjectSerializer(serializers.ModelSerializer):
    case = ProjectCaseSerializer(read_only=True)
    team = TeamSerializer(read_only=True)

    class Meta:
        model = Project
        fields = ('case', 'team')


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('title', 'status')


class MoveToSerializer(serializers.Serializer):
    target = serializers.ChoiceField(choices=Task.Statuses.choices)
