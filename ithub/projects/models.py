from django.db import models

from django.utils import timezone

from ithub.utils.choices import count_max_length


class Team(models.Model):
    name = models.CharField('Название', max_length=100)
    participants = models.ManyToManyField('students.Student', verbose_name='Участники')
    mentor = models.ForeignKey('mentors.Mentor', on_delete=models.CASCADE, verbose_name='Наставник')

    class Meta:
        verbose_name = 'Команда'
        verbose_name_plural = 'Команды'

    def __str__(self):
        return self.name


class Project(models.Model):
    case = models.ForeignKey('cases.Case', on_delete=models.CASCADE, verbose_name='Кейс')
    team = models.ForeignKey('Team', on_delete=models.CASCADE, verbose_name='Команда')
    start_dt = models.DateTimeField('Дата старта', default=timezone.now)
    end_dt = models.DateTimeField('Дата окончания', blank=True, null=True)
    is_finished = models.BooleanField('Завершен')
    retrospective = models.TextField('Ретроспектива')

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

    def __str__(self):
        return f'{self.team} - {self.case}'


class ChatMessage(models.Model):
    author = models.ForeignKey('users.User', on_delete=models.CASCADE, verbose_name='Автор')
    text = models.TextField('Текст')
    sent_dt = models.DateTimeField('Отправлено', default=timezone.now)
    project = models.ForeignKey('Project', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.sent_dt}: {self.author}'


class Task(models.Model):
    class Statuses(models.TextChoices):
        TODO = 'todo'
        IN_PROGRESS = 'in_progress'
        DONE = 'done'

    project = models.ForeignKey('Project', on_delete=models.CASCADE)
    title = models.CharField('Название', max_length=100)
    status = models.CharField('Статус', choices=Statuses.choices, max_length=count_max_length(Statuses))

    def __str__(self):
        return f'{self.project} - {self.title}'

    def move_to(self, target):
        self.status = target
        self.save(update_fields=['status'])
