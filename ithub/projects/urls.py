from django.urls import path

from . import views

app_name = 'projects'

urlpatterns = [
    path('<int:pk>/', views.ProjectView.as_view(), name='detail'),
    path('<int:project_pk>/board/tasks/', views.BoardView.as_view(), name='board'),
    path('<int:project_pk>/board/tasks/<int:pk>/move-to/', views.TaskMoveToView.as_view(), name='move_to'),
]
