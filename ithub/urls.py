from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('api/', include('ithub.api.urls')),
    path('api/users/', include('ithub.users.urls')),
    path('api/', include('ithub.professions.urls')),
    path('api/projects/', include('ithub.projects.urls')),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
